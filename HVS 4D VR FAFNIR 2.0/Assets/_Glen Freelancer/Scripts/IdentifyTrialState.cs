﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class IdentifyTrialState : MonoBehaviour 
{
    // Enter the filename in the editor inspector
    [SerializeField] private string fileName = "trialtype.vrConfig";  // <-- Default name

    private string wordToFind = "probe";


    private TrialState m_CurrentTrialState; // <--- do this because we want this class to only modify this variable
    public TrialState CurrentTrial  // But at the same time external classes will require to read the state
    {
        get
        {
            return m_CurrentTrialState;
        }
    }

    private void Awake()
    {
        LoadData(FilePath());
    }

    public bool devTest = false;
    private void Update()
    {
        if(devTest)
        {
            LoadData(FilePath());
            devTest = false;
        }
    }

    private void LoadData(string filePath)
    {
        if(!File.Exists(filePath))
        {
            Debug.LogError ("The file you're trying to load doesn't exist at path: " + filePath);
            return;
        }

        // File is closed after Using(){} 
        using (StreamReader reader = new StreamReader(filePath))
        {
            string line;
            while((line = reader.ReadLine()) != null)
            {
                if(!string.IsNullOrEmpty(line))
                {
                    if (line.Contains(wordToFind))
                    {
                        Debug.LogError("Found a match!");
                        m_CurrentTrialState = TrialState.Probe;
                        return; // <--- we found a word probe so let's just exit.
                    }
                }
            }
        }

        Debug.LogError("Didn't find match");
        // If we get here... we can assume that there was no word 'probe' found
        m_CurrentTrialState = TrialState.Regular;
    }

    private string FilePath()
    {
        return Application.persistentDataPath + "/" + fileName;
    }
}

public enum TrialState
{
    Regular,
    Probe
}
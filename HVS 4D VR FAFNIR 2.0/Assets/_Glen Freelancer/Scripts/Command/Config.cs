﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config : MonoBehaviour 
{
    private Vector3 VIEW_TRANSFORM = Vector3.zero;

    private static Config m_Instance;
    public static Config Instance
    {
        get
        {
            return m_Instance;
        }
    }

    private void Awake()
    {
        m_Instance = this;
    }
}

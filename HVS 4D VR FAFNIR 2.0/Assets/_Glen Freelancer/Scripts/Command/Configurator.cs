﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class Configurator : MonoBehaviour 
{
    public string sentence = "I went to the shop";

    public bool check = false;
    private void Update()
    {
        /*if (check)
        {
            if(sentence.Contains("shop"))
            {
               
            }
            check = false;
        }*/
    }

    private const string m_FilePath = "";
    public void Configure()
    {
        string line;
        using (StreamReader reader = new StreamReader(m_FilePath))
        {
            line = reader.ReadLine();

            if (!string.IsNullOrEmpty(line))
            {
                if (line.Contains("VIEW_TRANSFORM"))
                {
                    VIEW_TRANSFORM(line);
                }

                if (line.Contains("VIEW_OFFSET"))
                {
                    VIEW_OFFSET(line);
                }

                if (line.Contains("CONTROL_TRANSFORM"))
                {
                    CONTROL_TRANSFORM(line);
                }
                    
            }
            else
            {
                Debug.Log("Line is empty or null for performance reasons, try to reduce the amount of vertical spacing / empty lines in file");
            } 

        }
    }

    /// <summary>
    /// Changes the orientation of the default view. For example VIEW_TRANSFORM 0,-90,0
    /// Starts the experiment with the subject's viewpoint looking at their ‘feet’ even 
    /// though the headset is normal and horizontal.
    /// </summary>
    private void VIEW_TRANSFORM(string line)
    {
    }

    private void VIEW_OFFSET(string line)
    {
    }

    private void CONTROL_TRANSFORM(string line)
    {
      
    }
}

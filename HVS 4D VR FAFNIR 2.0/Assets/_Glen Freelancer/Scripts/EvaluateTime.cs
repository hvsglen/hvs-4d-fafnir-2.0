﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EvaluateTime : MonoBehaviour 
{
    [SerializeField] private EvaluateDistance distanceChecker;

	[SerializeField] private teleport teleportScript;
	[SerializeField] private Text counter;

	[SerializeField] private Color32 normalCounterColor = Color.white;
	[SerializeField] private Color32 overtimeCounterColor = Color.red;

	[SerializeField] private GameObject trialOverUIObj;
	private bool displayingTrialOver = false;

	private float currentTime = 0;

    public bool HasTimerAlreadyFinished = false;

	void Update()
	{
		if (teleportScript.timerRunning) 
		{
			currentTime += Time.deltaTime;

			if (HasReachedTotalTime ()) 
			{
				// if we're '!' not displaying trial text
				if (!displayingTrialOver) 
				{
                    if (!distanceChecker.HasAlreadyReachedTarget && !HasTimerAlreadyFinished)
                    {
                        trialOverUIObj.SetActive (true);

                        // Showing trial over text
                        displayingTrialOver = true;

                        HasTimerAlreadyFinished = true;
                    }
								// Changing the color
					counter.color = overtimeCounterColor;

					teleportScript.removeParticipantObj.SetActive (true);
				}

				//teleportScript.isEnding = true; // <--- not sure what else may reference this but, previous script did so I will do so
				// just to make sure I haven't broke anything.

				// Show the counter incrementing
				counter.text = (currentTime - teleportScript.totalTime).ToString("F1");


				teleportScript.ptf.canWrite = false;
			}
			else 
			{
				counter.color = normalCounterColor;

				counter.text = (teleportScript.totalTime - currentTime).ToString("F1");
			}
		}
	}

	private bool HasReachedTotalTime()
	{
		if (currentTime >= teleportScript.totalTime) 
		{
			return true;
		}

		return false;
	}



	public void ResetTimer()
	{
		currentTime = 0;
		counter.text = "0";
		counter.color = normalCounterColor;
		trialOverUIObj.SetActive (false);
        HasTimerAlreadyFinished = false;
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ReadFileMoveObject : MonoBehaviour
{
	// Load file
	// 6 digit coordinate system
	// Any number 

	// Object you want to move
	[SerializeField] private GameObject ObjectToMove;

	// Enter the filename in the editor inspector
	[SerializeField] private string fileName = "object1.vrConfig";  // <-- Default name

	[SerializeField] private bool useLocalPosition = false;	
	// Load file and set object in Start()
	private void Start()
	{
		if (ObjectToMove == null) 
		{
			ObjectToMove = gameObject;
			Debug.LogError ("Variable ObjectToMove == null, what ever gameobject this script is attached to, + " +
				" this script will now move it ('when called upon - LoadData()'");
		}

		LoadData(FilePath());
	}


	private void LoadData(string filePath)
	{
		if(!File.Exists(filePath))
		{
			Debug.LogError ("The file you're trying to load doesn't exist at path: " + filePath);
			return;
		}

		// File is closed after Using(){} 
		using (StreamReader reader = new StreamReader(filePath))
		{
			string line;
			while((line = reader.ReadLine()) != null)
			{

				if(!string.IsNullOrEmpty(line))
				{
					string[] pr = line.Split(new string[] { "," }, System.StringSplitOptions.None);

					Vector3 position = new Vector3(float.Parse(pr[0]), float.Parse(pr[1]), float.Parse(pr[2]));
					Vector3 rotation = new Vector3(float.Parse(pr[3]), float.Parse(pr[4]), float.Parse(pr[5]));
					if (useLocalPosition) 
					{
						ObjectToMove.transform.localPosition = position;
						ObjectToMove.transform.localEulerAngles = rotation;
					}
					else
					{
						ObjectToMove.transform.position = position;
						ObjectToMove.transform.eulerAngles = rotation;
					}

				}
			}
		}
	}

	private string FilePath()
	{
		return Application.persistentDataPath + "/" + fileName;
	}
}

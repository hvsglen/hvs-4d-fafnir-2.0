﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvaluateDistance : MonoBehaviour 
{
    // Identify current trial state
    [SerializeField] private EvaluateTime timer;
    [SerializeField] private IdentifyTrialState trialState;
	[SerializeField] private GameObject playerObj;
	[SerializeField] private GameObject platformObj;
	[SerializeField] private teleport teleportScript;

	[SerializeField] private GameObject endUIObj;

    public bool HasAlreadyReachedTarget = false;

	// Cache variable
	private float distanceToEnd;

	void Awake()
	{
        if (trialState == null)
            Debug.LogError("The script 'trialState' in EvaluateDistance.cs cannot be null,ensure you have attached a reference in the inspector");
        
		SetEndUIActivity (false);	// By default we wan't to ensure that the end text is not being displayed
	}

	void Update()
	{
		if (teleportScript.timerRunning == true) 
        {

			if (ObjectIsWithinRange ()) 
            {

                if (trialState == null)
                {
                    RegularTimeTrialEnd();
                }
                else
                {
                    if (trialState.CurrentTrial == TrialState.Probe)
                    {
                        ProbeTimeTrialEnd();
                    }
                    else
                    {
                        RegularTimeTrialEnd();
                    }
                }
			}
		}
	}

    private void ProbeTimeTrialEnd()
    {
        
        // keep moving, if the platform has reached its target 'probe'
        teleportScript.fps.canMove = true;

        // Timer... writing thing...keep recording positioninig if 'probe'
        teleportScript.ptf.canWrite = true;

        // no showing of platform reached..
        SetEndUIActivity (false);

        // no showing of remove participant
        teleportScript.removeParticipantObj.SetActive(false);

        //Debug.LogError("Probe time end");
    }

    private void RegularTimeTrialEnd()
    {
            // Stop timer
            teleportScript.timerRunning = false;

            // stop moving, if the platform has reached its target 
            teleportScript.fps.canMove = false;

            // Timer... writing thing...stop recording positionini
            teleportScript.ptf.canWrite = false;

            
            if (!timer.HasTimerAlreadyFinished && !HasAlreadyReachedTarget)
            {
                HasAlreadyReachedTarget = true;

                //  Showing of platform reached..
                SetEndUIActivity (true);
            }

            teleportScript.removeParticipantObj.SetActive(true);
            
            //  showing of remove participant
            //Debug.LogError("Regular time end");
    }

	private bool ObjectIsWithinRange()
	{
		distanceToEnd = Vector3.Distance (
			playerObj.transform.position,
			platformObj.transform.position);

		if (distanceToEnd <= teleportScript.endRadiusDistance) 
			return true;

		return false;
	} 

	private void SetEndUIActivity(bool active)
	{
		endUIObj.SetActive (active);
	}

	public void Reset()
	{
		SetEndUIActivity (false);
        HasAlreadyReachedTarget = false;
	}
}

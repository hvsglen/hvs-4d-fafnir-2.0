﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseApplication : MonoBehaviour 
{
	// Call this to quit the application.
	public void OnClick()
	{
		#if UNITY_EDITOR
		// Application.Quit() does not work in the editor so
		//UnityEditor.EditorApplication.isPlaying need to be set to false to end the game

		UnityEditor.EditorApplication.isPlaying = false;

		#else
		// This will get executed, if application is not running from Unity Editor
		Application.Quit();

		#endif
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
//using System.Runtime.Serialization.Formatters.Binary;

public class teleport : MonoBehaviour {

	[Header("Session Settings")]
	public string timerFile = "end.vrConfig";
	public float timerValue = 90f;
	public bool timerRunning = false;
	public string endRadiusFile = "platformRadius.vrConfig";
	public float endRadiusDistance;
	public bool endInsideDistance = true;

	public bool endInverseX = false;
	public bool endInverseZ = true;
	public string endPointFile;
	public Transform endPointTransform;

	public float totalTime;
	private float realDistance;

	[Header("Debug Values")]
	public float distanceToEnd;
	public float currentTime;

	[Header("Teleport Settings")]
	public bool useFiles = true;
	public TeleportSettings[] teleportSettings;

	//public bool isEnding = false;

	GameObject[] Teleport;				// All teleport locations
	[Header("UI Reference")]
	public Text TeleportText;			// The Text for the Teleport Names
	public GameObject fadeOutPanel;

	public FirstPersonController fps;
	public PositionToFile ptf;
	private bool inGame = false;

	public GameObject removeParticipantObj;
	[SerializeField] private EvaluateTime evaluateTime;
	[SerializeField] private EvaluateDistance evaluateDistance;
	void Start () 
	{

		removeParticipantObj.SetActive (false);
		ptf = FindObjectOfType(typeof(PositionToFile)) as PositionToFile;
		inGame = false;

		// No rotation at start
		fps = gameObject.GetComponent<FirstPersonController>();
		fps.canRotate = false;

		// Finds all the GameObject who has the tag named: Teleport in it, this is expensive operation.
		Teleport = GameObject.FindGameObjectsWithTag("Teleport").OrderBy(go => go.name).ToArray();

		// Game end
		string endPath = Application.persistentDataPath + "/" + endRadiusFile;
		if (System.IO.File.Exists (endPath)) {

			StreamReader esr = new StreamReader (endPath);
			string endData = esr.ReadLine ();
			if (endData != "") {
				float distance;
				float.TryParse (endData, out distanceToEnd);
				if (distance != null) 
				{
					realDistance = float.Parse (endData);
					endRadiusDistance = realDistance;
				}
			} else {
				//endRadiusDistance = endRadiusDistance;
			}

			if (realDistance < 0f) {
				endInsideDistance = false;
			} else {
				endInsideDistance = true;
			}

			esr.Close ();

		} else {

			FileStream efs = File.Open (endPath, FileMode.OpenOrCreate);
			string endString = endRadiusDistance.ToString("F6");
			StreamWriter ewriter = new StreamWriter (efs);
			ewriter.WriteLine (endString);
			ewriter.Flush ();
			ewriter.Close ();
		}

		// Timer
		string timerPath = Application.persistentDataPath + "/" + timerFile;

		if (System.IO.File.Exists (timerPath)) {

			StreamReader tsr = new StreamReader (timerPath);
			string timerData = tsr.ReadLine ();
			if (timerData != "") {

				float time;
				float.TryParse (timerData, out time);
				if (time != null) 
				{
					timerValue = totalTime;
					totalTime = time;
				}
			} else {
				totalTime = timerValue;
			}

			if (totalTime < 0f) {
				timerRunning = false;
			} else {
				timerRunning = true;
				fps.canMove = true;
			}

			tsr.Close ();

		} else {

			FileStream tfs = File.Open (timerPath, FileMode.OpenOrCreate);
			string timeString = timerValue.ToString("F6");
			StreamWriter twriter = new StreamWriter (tfs);
			twriter.WriteLine (timeString);
			twriter.Flush ();
			twriter.Close ();
		}

		currentTime = 0;

		Debug.Log ("Application data path location: " + Application.persistentDataPath + "/");

		TeleportText.text += "\n";

		if (useFiles == true) {
			// first create files and populate numbers based on what is in scene
			int n = 0;
			//TeleportText.text = "";
			foreach (TeleportSettings ts in teleportSettings) {

				string path = Application.persistentDataPath + "/" + ts.cooFileName;
				FileStream fs = File.Open (path, FileMode.OpenOrCreate);
				ts.fileData = true;

				if (fs.Length == 0) {

					ts.fileData = false;

					//string writeString = Teleport [n].transform.position.x + "," + Teleport [n].transform.position.y + "," + Teleport [n].transform.position.z;
					//writeString += "," + Teleport [n].transform.rotation.x + "," + Teleport [n].transform.rotation.y + "," + Teleport [n].transform.rotation.z;

					string writeString = "Location " + n + ",0,0,0,0,0,0";
					GameObject g = new GameObject ("Location " + n);
					ts.cooTransform = g.transform;

					TeleportText.text += "Undefined" + "\n";

					StreamWriter writer = new StreamWriter (fs);
					writer.WriteLine (writeString);
					writer.Flush ();
					writer.Close ();

				}

				n++;
				fs.Close ();
			}

			int r = 0;
			foreach (TeleportSettings ts in teleportSettings) {

				if (ts.fileData == false) {
					return;
				}

				string path = Application.persistentDataPath + "/" + ts.cooFileName;

				StreamReader sr = new StreamReader (path);
				string posData = sr.ReadLine ();

				string[] posPos = posData.Split (new string[] {","}, System.StringSplitOptions.None);

				GameObject g = new GameObject (ts.cooFileName);
				ts.cooTransform = g.transform;

				ts.cooTransform.name = posPos [0].ToString ();

				// create (Vector pos and rot)
				Vector3 vp = new Vector3 (float.Parse(posPos[1]), float.Parse(posPos[2]), float.Parse(posPos[3]));
				Vector3 vr = new Vector3 (float.Parse(posPos[4]), float.Parse(posPos[5]), float.Parse(posPos[6]));

				ts.cooTransform.position = VectorInverse(r, vp);
				ts.cooTransform.eulerAngles = vr;

				/*
				ts.cooTransform.position = new Vector3 (float.Parse(posPos[1]), float.Parse(posPos[2]), float.Parse(posPos[3]));
				ts.cooTransform.eulerAngles = new Vector3 (float.Parse(posPos[4]), float.Parse(posPos[5]), float.Parse(posPos[6]));
				*/
				TeleportText.text += ts.cooTransform.name + "\n";
				//n++;
				sr.Close ();

			}


			// endPos part
			string endPointPath = Application.persistentDataPath + "/" + endPointFile;

			if (!System.IO.File.Exists (endPointPath)) {

				FileStream fss = File.Open (endPointPath, FileMode.OpenOrCreate);
				if (fss.Length == 0) {
					string writePos = endPointTransform.position.x.ToString("F6") + "," + endPointTransform.position.y.ToString("F6") + "," + endPointTransform.position.z.ToString("F6");
					StreamWriter writeStream = new StreamWriter (fss);
					writeStream.WriteLine (writePos);
					writeStream.Flush ();
					writeStream.Close ();
				}
				fss.Close ();

			} else {

				StreamReader esr = new StreamReader (endPointPath);
				string endposData = esr.ReadLine ();
				string[] eposPos = endposData.Split (new string[] {","}, System.StringSplitOptions.None);

				Vector3 ept = new Vector3 (float.Parse(eposPos[0]), float.Parse(eposPos[1]), float.Parse(eposPos[2]));
				endPointTransform.position = VectorInverseEnd(ept);

				//endPointTransform.position = new Vector3 (float.Parse(eposPos[0]), float.Parse(eposPos[1]), float.Parse(eposPos[2]));

			}

			if (teleportSettings[0].cooFileName.Length > 0) {
				transform.localPosition = teleportSettings[0].cooTransform.position;
				transform.localRotation = teleportSettings[0].cooTransform.rotation;
				timerRunning = false;
			}

		}

		// Teleports the user to the right destination
		transform.localPosition = Teleport[0].transform.position;

	}

	// Update is called once per frame
	void Update () {

		if (useFiles == false) {
			// when shift and a number is placed it wil teleport to the destinated place.
			for (int i = 1; i <= Teleport.Length; i++) {
				if (Input.GetKeyDown (i.ToString ())) {
					transform.position = Teleport [i - 1].transform.position;
				}
			}
		} else {
			// when shift and a number is placed it wil teleport to the destinated place.
			for (int i = 0; i <= teleportSettings.Length; i++) {
				// User presses key
				if (Input.GetKeyDown (i.ToString ())) {

					evaluateTime.ResetTimer ();
					evaluateDistance.Reset ();
					removeParticipantObj.SetActive (false);
					// Resetting timer
					currentTime = 0f;
					if (i == 0) 
					{
						timerRunning = false;
						ptf.canWrite = false;
					} else {
						timerRunning = true;
						ptf.canWrite = true;
					}   

					fps.canRotate = false;
					fps.m_MouseLook.Reset (teleportSettings [i].cooTransform.rotation, Quaternion.identity);
					transform.position = teleportSettings [i].cooTransform.position;
					transform.rotation = teleportSettings [i].cooTransform.rotation;
					fps.canRotate = true;

				}
			}
		}
	}


	public void Restart() {

		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);

	}

	Vector3 VectorInverse(int settingsIndex, Vector3 v) {



		float vx = 0f;
		float vy = 0f;
		float vz = 0f;

		if (teleportSettings[settingsIndex].inverseX == true) {
			vx = v.x * -1f; // inverse any x
			//Debug.Log ("InvertX, settingsIndex: " + settingsIndex + ", Vector3: " + v);
		} else {
			vx = v.x;
		}

		vy = v.y;

		if (teleportSettings[settingsIndex].inverseZ == true) {
			vz = v.z * -1f;
			//Debug.Log ("InvertZ, settingsIndex: " + settingsIndex + ", Vector3: " + v);
		} else {
			vz = v.z;
		}


		//Vector3 rV = Vector3.zero;

		//if (teleportSettings [settingsIndex].useOriginalValue == true) {
		//	rV = v;
		//} else {
		Vector3	rV = new Vector3 (vx, vy, vz);
		//}

		return rV;

	}

	Vector3 VectorInverseEnd(Vector3 v) {

		float vx = 0f;
		float vy = 0f;
		float vz = 0f;

		if (endInverseX == true) {
			vx = v.x * -1f; // inverse any x
		} else {
			vx = v.x;
		}

		vy = v.y;

		if (endInverseZ == true) {
			vz = v.z * -1f;
		} else {
			vz = v.z;
		}

		return new Vector3 (vx, vy, vz);

	}

}


[System.Serializable]
public class TeleportSettings
{
	[Header("Inverse Override")]
	public bool inverseX;
	public bool inverseZ;
	//public bool useOriginalValue;

	[Header("Data Config")]
	public string cooFileName;
	public Transform cooTransform;
	public bool fileData;
}




﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatTriggerListener : ListenForTrigger 
{
    public Animator m_AnimController;

    public List<string> m_ListOfAnimationNames;

    public override void ReceivedTrigger()
    {
        int id = Random.Range(0, m_ListOfAnimationNames.Count);
        m_AnimController.SetTrigger(m_ListOfAnimationNames[id]);
    }    

    private void OnTriggerStay(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (Input.GetMouseButtonDown(0))
            {
                ReceivedTrigger();
            }
        }
    }
}

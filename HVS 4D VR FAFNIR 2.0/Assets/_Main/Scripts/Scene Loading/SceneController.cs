﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class MoveableObject
{
    public GameObject Object;
    public Vector3 Position;
    public Vector3 Rotation;
    public Vector3 Scale = Vector3.one;
}

[System.Serializable]
public class MoveableObjectDictionary
{
    public List<MoveableObject> List;

    public void AddObjectField()
    {
        if (List == null)
            List = new List<MoveableObject>();

        List.Add(new MoveableObject()
            {
               Object = null, 
                Position = Vector3.zero,
            });
    }

    public void RemoveObjectField(int index)
    {
        if (List == null || index >= List.Count)
            return;

        List.RemoveAt(index);
    }
}   

[System.Serializable]
public class SceneController : MonoBehaviour 
{
    /*public string m_SceneOne;
    public string m_SceneTwo;*/

    public GameObject HumanScaleFpsController;
    public GameObject StartPostionObj;
    private GameObject m_StartPositionObject;
    public GameObject EndPositionObj;
    private GameObject m_EndPositionObject;

    public List<string> SceneNames;
    public List<bool> SceneIsActive;
    public List<string> SceneInputKey;
    public List<Vector3> SceneStartPosition;
    public List<Vector3> SceneEndPosition;
    public List<bool> SceneShowGlobalEndUI;
    
    public List<bool> SceneShouldUseRatScale;
    public List<MoveableObjectDictionary> SceneMoveableObjects;

    public GameObject EndUIObj;
    private bool m_ShowGlobalEndUI = false;

    private List<GameObject> m_GameObjectsToDestroy = new List<GameObject>();

    public void AddSceneField()
    {
        if (SceneNames == null)
            SceneNames = new List<string>();

        if (SceneIsActive == null)
            SceneIsActive = new List<bool>();

        if (SceneInputKey == null)
            SceneInputKey = new List<string>();

        if (SceneShouldUseRatScale == null)
            SceneShouldUseRatScale = new List<bool>();

        if (SceneMoveableObjects == null)
            SceneMoveableObjects = new List<MoveableObjectDictionary>();

        if (SceneStartPosition == null)
            SceneStartPosition = new List<Vector3>();

        if (SceneEndPosition == null)
            SceneEndPosition = new List<Vector3>();

        if (SceneShowGlobalEndUI == null)
            SceneShowGlobalEndUI = new List<bool>();
        
        SceneNames.Add("");
        SceneIsActive.Add(false);
        SceneInputKey.Add("");
        SceneShouldUseRatScale.Add(false);
        SceneMoveableObjects.Add(null);
        SceneStartPosition.Add(Vector3.zero);
        SceneEndPosition.Add(Vector3.zero);
        SceneShowGlobalEndUI.Add(false);
    }

    public void RemoveSceneField(int index)
    {
        if (SceneNames != null && index < SceneNames.Count)
            SceneNames.RemoveAt(index);

        if (SceneIsActive != null && index < SceneIsActive.Count)
            SceneIsActive.RemoveAt(index);

        if (SceneInputKey != null && index < SceneInputKey.Count)
            SceneInputKey.RemoveAt(index);
    
        if (SceneShouldUseRatScale != null && index < SceneShouldUseRatScale.Count)
            SceneShouldUseRatScale.RemoveAt(index);

        if (SceneMoveableObjects != null && index < SceneMoveableObjects.Count)
            SceneMoveableObjects.RemoveAt(index);

        if (SceneStartPosition != null && index < SceneStartPosition.Count)
            SceneStartPosition.RemoveAt(index);

        if (SceneEndPosition != null && index < SceneEndPosition.Count)
            SceneEndPosition.RemoveAt(index);
       
        if (SceneShowGlobalEndUI != null && index < SceneShowGlobalEndUI.Count)
            SceneShowGlobalEndUI.RemoveAt(index);

    }

    private void Update()
    {
        for (int i = 0; i < SceneNames.Count; i++)
        {
            if (SceneInputKey[i] != string.Empty &&
               Input.GetKeyDown(SceneInputKey[i]))
            {
                LoadScene(SceneNames[i]);
            }

        }
    }

    private void LoadScene(string sceneName)
    {
        for (int i = 0; i < SceneNames.Count; i++)
        {
            if (SceneNames[i] == sceneName)
            {
                m_ShowGlobalEndUI = SceneShowGlobalEndUI[i];
                EndUIObj.SetActive(false);

                if (SceneIsActive[i])
                {
                    Debug.Log("Cannot load the same scene in more than once");
                    return;
                }
                int index = i;
                SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
               
                ClearObjects();
                SpawnMoveableObjects(index);
                SceneIsActive[i] = true;


                if (SceneShouldUseRatScale[i])
                    HumanScaleFpsController.SetActive(false);
                else
                    HumanScaleFpsController.SetActive(true);

                if(!SceneShouldUseRatScale[index])
                    HumanScaleFpsController.transform.position = SceneStartPosition[index];

                SpawnStartAndEndObjects(index);
            }
            else
            {
                if (SceneIsActive[i] == true)
                {
                    UnloadScene(SceneNames[i]);
                    SceneIsActive[i] = false;
                }
            }
        }
    }

    /// <summary>
    /// Spawns the start and end objects.
    /// </summary>
    /// <param name="">.</param>
    private void SpawnStartAndEndObjects(int index)
    {
        if (SceneStartPosition != null && index < SceneStartPosition.Count)
        {
            if (m_StartPositionObject == null)
                m_StartPositionObject = (GameObject)Instantiate(StartPostionObj);

            if (m_EndPositionObject == null)
                m_EndPositionObject = (GameObject)Instantiate(EndPositionObj);

            m_StartPositionObject.GetComponent<PlayerStartPosition>().Setup(SceneStartPosition[index]);

         

            m_EndPositionObject.GetComponent<PlayerEndPosition>().Setup(SceneEndPosition[index], this);
        }
    }
    private void ClearObjects()
    {
        if (m_GameObjectsToDestroy == null)
            return;

        if (m_GameObjectsToDestroy.Count == 0)
            return;

        for (int i = 0; i < m_GameObjectsToDestroy.Count; i++)
        {
            Destroy(m_GameObjectsToDestroy[i]);
        }

        m_GameObjectsToDestroy.Clear();
    }
    private void SpawnMoveableObjects(int sceneID)
    {
        if (SceneMoveableObjects[sceneID] == null || SceneMoveableObjects[sceneID].List == null || SceneMoveableObjects[sceneID].List.Count == 0)
            return;
        
        for (int i = 0; i < SceneMoveableObjects[sceneID].List.Count; i++)
        {
            if (SceneMoveableObjects[sceneID].List[i].Object == null)
                continue;

            GameObject obj = Instantiate(SceneMoveableObjects[sceneID].List[i].Object);
            obj.transform.localPosition = SceneMoveableObjects[sceneID].List[i].Position;
            obj.transform.localEulerAngles = SceneMoveableObjects[sceneID].List[i].Rotation;
            obj.transform.localScale = SceneMoveableObjects[sceneID].List[i].Scale;
            m_GameObjectsToDestroy.Add(obj);
        }
    }

    private void UnloadScene(string sceneName)
    {
        SceneManager.UnloadScene(sceneName);
    }

    public void PlayerReachedEndObject()
    {
        Debug.Log("Player reached the end");

        if (m_ShowGlobalEndUI)
        {
            EndUIObj.SetActive(true);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TigerTriggerListener : ListenForTrigger 
{
    public Animator m_Animator;
    public override void ReceivedTrigger()
    {
        m_Animator.SetTrigger("Jump");
    }
}

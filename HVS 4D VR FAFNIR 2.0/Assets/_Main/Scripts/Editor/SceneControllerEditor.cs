﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SceneController))]
public class SceneControllerEditor : Editor
{
    private SceneController Controller;

    private SerializedObject GetTarget;

    private void OnEnable()
    {
        Controller = (SceneController)target;
        GetTarget = new SerializedObject(Controller);
    }

    public override void OnInspectorGUI()
    {
        GetTarget.Update();

        Controller.HumanScaleFpsController = (GameObject)EditorGUILayout.ObjectField("Human Scale FPS Controller", Controller.HumanScaleFpsController, typeof(Object), true);
        Controller.StartPostionObj = (GameObject)EditorGUILayout.ObjectField("StartPostionObj", Controller.StartPostionObj, typeof(Object), true);
        Controller.EndPositionObj = (GameObject)EditorGUILayout.ObjectField("EndPositionObj", Controller.EndPositionObj, typeof(Object), true);
        Controller.EndUIObj = (GameObject)EditorGUILayout.ObjectField("End UI Object", Controller.EndUIObj, typeof(Object));


        GUI.color = Color.green;
        if (GUILayout.Button("Add scene"))
        {
            Controller.AddSceneField();
        }
                    
        GUI.color = Color.white;
        if   (Controller != null
           && Controller.SceneNames != null
           && Controller.SceneNames.Count > 0)
        {
            for (int i = 0; i < Controller.SceneNames.Count; i++)
            {
                GUI.color = Color.cyan;
                Controller.SceneNames[i] = EditorGUILayout.TextField("Scene name: ", Controller.SceneNames[i]);
                GUI.color = Color.white;
                Controller.SceneInputKey[i] = EditorGUILayout.TextField("Input key to activate scene: ", Controller.SceneInputKey[i]);
                Controller.SceneShouldUseRatScale[i] = EditorGUILayout.Toggle( "Use rat scale: ",Controller.SceneShouldUseRatScale[i]);


                GUI.color = Color.magenta;
                if (!Controller.SceneShouldUseRatScale[i])
                {
                    Controller.SceneShowGlobalEndUI[i] = EditorGUILayout.Toggle("Show global end ui", Controller.SceneShowGlobalEndUI[i]);
                    Controller.SceneStartPosition[i] = EditorGUILayout.Vector3Field("Player start position", Controller.SceneStartPosition[i]);
                    Controller.SceneEndPosition[i] = EditorGUILayout.Vector3Field("Player end position", Controller.SceneEndPosition[i]);
                }

                GUI.color = Color.green;
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Add moveable object"))
                {
                    Controller.SceneMoveableObjects[i].AddObjectField();
                }
                GUILayout.EndHorizontal();

                if (Controller.SceneMoveableObjects != null)
                {
                   
                    if (Controller.SceneMoveableObjects[i] != null
                       && Controller.SceneMoveableObjects[i].List != null
                       && Controller.SceneMoveableObjects[i].List.Count > 0)
                    {
                        for (int j  = 0; j < Controller.SceneMoveableObjects[i].List.Count; j++) 
                        {
                            

                            GUI.color = Color.white;
                            Controller.SceneMoveableObjects[i].List[j].Object = (GameObject)EditorGUILayout.ObjectField("Moveable gameobject: ", Controller.SceneMoveableObjects[i].List[j].Object, typeof(Object), true);
                            Controller.SceneMoveableObjects[i].List[j].Position = EditorGUILayout.Vector3Field("Position: ", Controller.SceneMoveableObjects[i].List[j].Position);
                            Controller.SceneMoveableObjects[i].List[j].Rotation = EditorGUILayout.Vector3Field("Rotation: ", Controller.SceneMoveableObjects[i].List[j].Rotation);
                            Controller.SceneMoveableObjects[i].List[j].Scale = EditorGUILayout.Vector3Field("Scale: ", Controller.SceneMoveableObjects[i].List[j].Scale);

                            GUILayout.BeginHorizontal();
                            GUILayout.FlexibleSpace();
                            GUI.color = Color.red;
                            if (GUILayout.Button("Remove moveable object"))
                            {
                                Controller.SceneMoveableObjects[i].RemoveObjectField(j);
                            }
                            GUILayout.EndHorizontal();       
                        }
                           
                    }
                   
                }
                GUI.color = Color.red;
                if (GUILayout.Button("Remove scene"))
                {
                    Controller.RemoveSceneField(i);
                }
            }
        }

        Undo.RecordObject(target, "Changed Area Of Effect");
        GetTarget.ApplyModifiedProperties();

    }
}

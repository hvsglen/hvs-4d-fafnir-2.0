﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStartPosition : MonoBehaviour 
{
    // Don't want to keep firing ontriggerenter func
    private bool triggered = false;

    public void Setup(Vector3 spawnPosition)
    {
        triggered = false;
        transform.localPosition = spawnPosition;        
    }

    void OnTriggerEnter(Collider coll)
    {
        if (triggered)
            return;
    }
}

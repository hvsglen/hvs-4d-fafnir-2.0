﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEndPosition : MonoBehaviour 
{
    // Don't want to keep firing ontriggerenter func
    private bool triggered = false;
    private SceneController Controller;

    public void Setup(Vector3 spawnPosition, SceneController controller)
    {
        Controller = controller;

        triggered = false;

        transform.localPosition = spawnPosition;
    }

    void OnTriggerEnter(Collider coll)
    {
        if (triggered)
            return;

        triggered = true;


        Controller.PlayerReachedEndObject();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour 
{
    public List<ListenForTrigger> m_ListOfTriggersListeners;

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            for (int i = 0; i < m_ListOfTriggersListeners.Count; i++)
            {
                m_ListOfTriggersListeners[i].ReceivedTrigger();
            }
        }
    }
}

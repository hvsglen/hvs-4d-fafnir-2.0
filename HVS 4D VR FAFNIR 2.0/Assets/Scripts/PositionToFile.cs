using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class PositionToFile : MonoBehaviour {

	[Header("Inverse Vectors?")]
	public bool inverseX = true;
	public bool inverseZ = true;

	[Header("Other settings")]
	public Transform referencedObject;
	public string fileName = "userLocation.vrBuffer";
	public float writeDelay = 0.1f;
	public bool rewriteFile = true;

	private StreamWriter writer;
	private Transform refObjTransform;

	public bool canWrite = false;

	void Start() {
		refObjTransform = referencedObject;
		StartCoroutine (WritePosition());
	}

	void OnEnable () 
	{
		//StartCoroutine (WritePosition());
	}

	void OnDisable ()
	{
		StopAllCoroutines();
		writer.Close();
	}

	IEnumerator WritePosition ()
	{
		//BinaryFormatter bf = new BinaryFormatter ();
		string path = Application.persistentDataPath + "/" + fileName;
		FileStream fs = File.Open(path, FileMode.OpenOrCreate);

		if (rewriteFile)
		{
			fs.SetLength(0);
			fs.Flush();
		}

		print ("Start logging position to " + path);
		writer = new StreamWriter(fs);

		while (true)
		{
			if (canWrite == true) {
				Debug.Log ("Writing");
				string writeString = "";

				Vector3 inversedV = VectorInverse (refObjTransform.position);
				writeString += inversedV.x.ToString ("F6") + ",";
				writeString += inversedV.y.ToString ("F6") + ",";
				writeString += inversedV.z.ToString ("F6");

				writer.WriteLine (writeString);
				writer.Flush ();
			} else {
				Debug.Log ("not writing");
			}
			yield return new WaitForSeconds(writeDelay);
		}

		/*
		while (true)
		{
			if (canWrite == true) {
				string writeString = "";
				writeString += refObjTransform.position.x.ToString ("F6") + ",";
				writeString += refObjTransform.position.y.ToString ("F6") + ",";
				writeString += refObjTransform.position.z.ToString ("F6");

				writer.WriteLine (writeString);
				writer.Flush ();
			}
			yield return new WaitForSeconds(writeDelay);
		}
		*/
	}


	Vector3 VectorInverse(Vector3 v) {

		float vx = 0f;
		float vy = 0f;
		float vz = 0f;

		if (inverseX == true) {
			vx = v.x * -1f; // inverse any x
		} else {
			vx = v.x;
		}

		vy = v.y;

		if (inverseZ == true) {
			vz = v.z * -1f; // inverse Z
		} else {
			vz = v.z;
		}

		return new Vector3 (vx, vy, vz);

	}
		
}